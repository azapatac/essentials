﻿namespace Essentials
{
    using System;
    using System.Linq;
    using System.Text;
    using Xamarin.Essentials;
    using Xamarin.Forms;

    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"AppName: {AppInfo.Name}");
            builder.AppendLine($"PackageName: {AppInfo.PackageName}");
            builder.AppendLine($"VersionString: {AppInfo.VersionString}");
            builder.AppendLine($"BuildString: {AppInfo.BuildString}");

            Application.Current.MainPage.DisplayAlert("AppInfo", builder.ToString(), "Ok");
        }

        void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            Application.Current.MainPage.DisplayAlert("Connectivity", Connectivity.NetworkAccess.ToString(), "Ok");            
        }

        async void Button_Clicked_2(System.Object sender, System.EventArgs e)
        {
            var location = new Location(47.645160, -122.1306032);
            var options = new MapLaunchOptions { Name = "Microsoft Building 25" };

            try
            {
                await Map.OpenAsync(location, options);
            }
            catch (Exception ex)
            {
                // No map application available to open
            }
        }

        async void Button_Clicked_3(System.Object sender, System.EventArgs e)
        {
            var status = await Permissions.RequestAsync<Permissions.Camera>();
            await Application.Current.MainPage.DisplayAlert("Permissions", status.ToString(), "Ok");
        }

        async void Button_Clicked_4(System.Object sender, System.EventArgs e)
        {
            await Share.RequestAsync(new ShareTextRequest
            {
                Uri = "https://net-university.ninja/",
                Title = ".Net University"
            });
        }

        async void Button_Clicked_5(System.Object sender, System.EventArgs e)
        {
            var locales = await TextToSpeech.GetLocalesAsync();

            await TextToSpeech.SpeakAsync("Welcome to .Net University", new SpeechOptions { Pitch = 0.1f, Locale = locales.FirstOrDefault(x=> x.Name.Contains("en-US"))});
        }

        async void Button_Clicked_6(System.Object sender, System.EventArgs e)
        {
            BrowserLaunchOptions browserLaunchOptions = new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                Flags = BrowserLaunchFlags.PresentAsFormSheet,
                PreferredControlColor = System.Drawing.Color.WhiteSmoke,
                PreferredToolbarColor = System.Drawing.Color.FromArgb(Convert.ToInt32("00A0FF", 16)),
            };

            await Browser.OpenAsync("https://docs.microsoft.com/en-us/xamarin/essentials/", browserLaunchOptions);

        }
    }
}